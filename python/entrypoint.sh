#!/usr/bin/env sh
exec gunicorn -b "0.0.0.0:$PORT" -w 4 -k uvicorn.workers.UvicornWorker --timeout 0  main:app