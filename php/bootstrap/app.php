<?php

require_once __DIR__.'/../vendor/autoload.php';

use Dotenv\Dotenv;

// load environment variables from our secrets.env file rather than from .env which is the default
$dotenv = Dotenv::createMutable(__DIR__.'/../..', 'secrets.env');
$dotenv->safeLoad();
$dotenv->required(['CLIENT_ID', 'CLIENT_SECRET']);

(new Laravel\Lumen\Bootstrap\LoadEnvironmentVariables(
  dirname(__DIR__)
))->bootstrap();

date_default_timezone_set(env('APP_TIMEZONE', 'UTC'));

/*
|--------------------------------------------------------------------------
| Create The Application
|--------------------------------------------------------------------------
*/

$app = new Laravel\Lumen\Application(dirname(__DIR__));

/*
|--------------------------------------------------------------------------
| Register Config Files
|--------------------------------------------------------------------------
*/

$app->configure('app');

// create our Photo API Service as a singleton
$app->singleton(App\Http\Services\PhotoAPIService::class);

/*
|--------------------------------------------------------------------------
| Load The Application Routes
|--------------------------------------------------------------------------
*/

$app->router->group([
  'namespace' => 'App\Http\Controllers',
], function ($router) {
  require __DIR__.'/../routes/web.php';
});

return $app;
