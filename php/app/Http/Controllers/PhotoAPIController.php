<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Symfony\Component\HttpFoundation\Response;

use App\Http\Services\PhotoAPIService;


class PhotoAPIController extends BaseController
{
  /**
   * Ensure that this controller is given a reference to a PhotoAPIService.
   */
  public function __construct(PhotoAPIService $photoAPIService)
  {
    $this->photoAPIService = $photoAPIService;
  }

  /**
   * Uses the PhotoAPIService to get the content of a photo, and returns the relevant
   * response. If the call to the Photo API has failed this outputs the error in plain
   * text, else it will output the content of the photo, setting the content-type based
   * on the content type of the response from the Photo API.
   */
  public function getPhoto($identifier)
  {
    $res = $this->photoAPIService->getPhotoContent($identifier);
    $body = $res->getBody();
    $statusCode = $res->getStatusCode();

    if ($statusCode !== 200) {
      return new Response('Photo API responded with ' . $statusCode . ': ' . $body, $statusCode);
    }

    $response = new Response($body, 200);
    $response->headers->set('Content-Type', $res->getHeaders()['Content-Type']);

    return $response;
  }
}
